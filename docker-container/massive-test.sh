#!/bin/bash

DOCKER_COMPOSE_LOG=~/Downloads/logs/docker-compose.log

DATE=$(TZ='America/Vancouver' date '+%Y-%m-%d')
export OVERRIDE_LOG_ROOT=$DATE

function install_tools() {
  # check if sar is installed
  if ! command -v sar &> /dev/null
  then
    echo "sar could not be found"
    echo "installing sysstat"

    sudo apt install sysstat
  fi
}

install_tools

function run() {
  local times=$1
  echo "Total to run: $times times"
  
  for ((i=1; i<=$times; i++))
  do
    echo "Running: $i"

    NOW=$(TZ='America/Vancouver' date '+%Y-%m-%d_%H-%M-%S')
    export OVERRIDE_LOG_PATH=$NOW

    source ../ros2-topic-message-pressure-test/settings.sh
    source ../ros2-topic-message-pressure-test/common.sh

    echo "LOG in Container: $LOG_DIR"
    # e.g. /home/eli.deng/logs/2024-04-12/2024-04-12_18-00-26-[1_10_reliable_keep_last_10]

    HOST_LOG_DIR=$(echo "$LOG_DIR" | sed "s#.*\/logs#${HOME}/Downloads/logs#g")

    echo "LOG in Host PC: $HOST_LOG_DIR"
    mkdir -p $HOST_LOG_DIR

    # clear
    docker compose down

    # "sar" stands for "System Activity Reporter. 
    # sudo apt install sysstat

    # start log cpu usage info in backgroud
    sar -u 1 > $HOST_LOG_DIR/sar.txt &
    pid=$!

    echo $NOW         | tee --append $DOCKER_COMPOSE_LOG
    docker compose up | tee --append $DOCKER_COMPOSE_LOG
    echo "-----"      | tee --append $DOCKER_COMPOSE_LOG

    # stop log cpu usage
    kill -SIGINT $pid

  done
}

# export OVERRIDE_OVERLAP_TIME=30 # in seconds


export OVERRIDE_QOS_RELIABILITY=reliable # system_default,reliable,best_effort,unknown
export OVERRIDE_QOS_HISTORY=keep_last # system_default,keep_last,keep_all,unknown
export OVERRIDE_QOS_DEPTH=10
export OVERRIDE_LOG_DIR_POSTFIX='' # will be appended to the log directory name

numbers=(1 2 4 6 8 10 12 14 16 18 20 22 24 26 28 30 32 34 36 38 40 42 44 46 48 50)
rates=(10 50 100 200 300 400 500 600 700 800 900 1000 1500 2000)

# numbers=(1)
# rates=(10)

# create the root log path
mkdir -p ~/Downloads/logs/

for number in "${numbers[@]}"
do
  for rate in "${rates[@]}"
  do
    export OVERRIDE_NUMBER=$number # number of pub+sub pairs
    export OVERRIDE_RATE=$rate # hz
    run 1
  done

  mkdir -p  ~/Downloads/logs/$DATE
done

python3   ~/code/eli-sanctuary/miscellaneous-scripts/ros2-topic-message-pressure-test/message-lost-statistic.py ~/Downloads/logs/$DATE ~/Downloads/logs/$DATE/$DATE-table.csv \
          "number_of_critical_gap"              ~/Downloads/logs/$DATE/$DATE-matrix-critical_gap.csv \
          "number_of_gap"                       ~/Downloads/logs/$DATE/$DATE-matrix-total_gap.csv \
          "cpu_max"                             ~/Downloads/logs/$DATE/$DATE-matrix-cpu_max.csv \
          "cpu_average"                         ~/Downloads/logs/$DATE/$DATE-matrix-cpu_average.csv \
          "number_of_beginning_part_missing"    ~/Downloads/logs/$DATE/$DATE-matrix-beginning_miss.csv \
          "number_of_complete_missing"          ~/Downloads/logs/$DATE/$DATE-matrix-complete_miss.csv \
          "overall_lost_rate"                   ~/Downloads/logs/$DATE/$DATE-matrix-overall_lost_rate.csv


# chanage the owner for all logs
sudo chown -R  $(id -u):$(id -g) ~/Downloads/logs/

# # ----- change NUMBER -----
# export OVERRIDE_NUMBER=1 # number of pub+sub pairs
# export OVERRIDE_RATE=200 # hz
# export OVERRIDE_QOS_RELIABILITY=reliable # system_default,reliable,best_effort,unknown
# export OVERRIDE_QOS_HISTORY=keep_last # system_default,keep_last,keep_all,unknown
# export OVERRIDE_QOS_DEPTH=10
# export OVERRIDE_LOG_DIR_POSTFIX='' # will be appended to the log directory name
# run 5

# export OVERRIDE_NUMBER=5 # number of pub+sub pairs
# export OVERRIDE_RATE=200 # hz
# export OVERRIDE_QOS_RELIABILITY=reliable # system_default,reliable,best_effort,unknown
# export OVERRIDE_QOS_HISTORY=keep_last # system_default,keep_last,keep_all,unknown
# export OVERRIDE_QOS_DEPTH=10
# export OVERRIDE_LOG_DIR_POSTFIX='' # will be appended to the log directory name
# run 5

# export OVERRIDE_NUMBER=25 # number of pub+sub pairs
# export OVERRIDE_RATE=200 # hz
# export OVERRIDE_QOS_RELIABILITY=reliable # system_default,reliable,best_effort,unknown
# export OVERRIDE_QOS_HISTORY=keep_last # system_default,keep_last,keep_all,unknown
# export OVERRIDE_QOS_DEPTH=10
# export OVERRIDE_LOG_DIR_POSTFIX='' # will be appended to the log directory name
# run 5

# export OVERRIDE_NUMBER=50 # number of pub+sub pairs
# export OVERRIDE_RATE=200 # hz
# export OVERRIDE_QOS_RELIABILITY=reliable # system_default,reliable,best_effort,unknown
# export OVERRIDE_QOS_HISTORY=keep_last # system_default,keep_last,keep_all,unknown
# export OVERRIDE_QOS_DEPTH=10
# export OVERRIDE_LOG_DIR_POSTFIX='' # will be appended to the log directory name
# run 5


# # ----- change NUMBER (with double depth)-----
# export OVERRIDE_NUMBER=1 # number of pub+sub pairs
# export OVERRIDE_RATE=200 # hz
# export OVERRIDE_QOS_RELIABILITY=reliable # system_default,reliable,best_effort,unknown
# export OVERRIDE_QOS_HISTORY=keep_last # system_default,keep_last,keep_all,unknown
# export OVERRIDE_QOS_DEPTH=20
# export OVERRIDE_LOG_DIR_POSTFIX='' # will be appended to the log directory name
# run 5

# export OVERRIDE_NUMBER=5 # number of pub+sub pairs
# export OVERRIDE_RATE=200 # hz
# export OVERRIDE_QOS_RELIABILITY=reliable # system_default,reliable,best_effort,unknown
# export OVERRIDE_QOS_HISTORY=keep_last # system_default,keep_last,keep_all,unknown
# export OVERRIDE_QOS_DEPTH=20
# export OVERRIDE_LOG_DIR_POSTFIX='' # will be appended to the log directory name
# run 5

# export OVERRIDE_NUMBER=25 # number of pub+sub pairs
# export OVERRIDE_RATE=200 # hz
# export OVERRIDE_QOS_RELIABILITY=reliable # system_default,reliable,best_effort,unknown
# export OVERRIDE_QOS_HISTORY=keep_last # system_default,keep_last,keep_all,unknown
# export OVERRIDE_QOS_DEPTH=20
# export OVERRIDE_LOG_DIR_POSTFIX='' # will be appended to the log directory name
# run 5

# export OVERRIDE_NUMBER=50 # number of pub+sub pairs
# export OVERRIDE_RATE=200 # hz
# export OVERRIDE_QOS_RELIABILITY=reliable # system_default,reliable,best_effort,unknown
# export OVERRIDE_QOS_HISTORY=keep_last # system_default,keep_last,keep_all,unknown
# export OVERRIDE_QOS_DEPTH=20
# export OVERRIDE_LOG_DIR_POSTFIX='' # will be appended to the log directory name
# run 5



# # ----- change NUMBER (with higher 500 HZ)-----
# export OVERRIDE_NUMBER=1 # number of pub+sub pairs
# export OVERRIDE_RATE=500 # hz
# export OVERRIDE_QOS_RELIABILITY=reliable # system_default,reliable,best_effort,unknown
# export OVERRIDE_QOS_HISTORY=keep_last # system_default,keep_last,keep_all,unknown
# export OVERRIDE_QOS_DEPTH=10
# export OVERRIDE_LOG_DIR_POSTFIX='' # will be appended to the log directory name
# run 5

# export OVERRIDE_NUMBER=5 # number of pub+sub pairs
# export OVERRIDE_RATE=500 # hz
# export OVERRIDE_QOS_RELIABILITY=reliable # system_default,reliable,best_effort,unknown
# export OVERRIDE_QOS_HISTORY=keep_last # system_default,keep_last,keep_all,unknown
# export OVERRIDE_QOS_DEPTH=10
# export OVERRIDE_LOG_DIR_POSTFIX='' # will be appended to the log directory name
# run 5

# export OVERRIDE_NUMBER=25 # number of pub+sub pairs
# export OVERRIDE_RATE=500 # hz
# export OVERRIDE_QOS_RELIABILITY=reliable # system_default,reliable,best_effort,unknown
# export OVERRIDE_QOS_HISTORY=keep_last # system_default,keep_last,keep_all,unknown
# export OVERRIDE_QOS_DEPTH=10
# export OVERRIDE_LOG_DIR_POSTFIX='' # will be appended to the log directory name
# run 5

# export OVERRIDE_NUMBER=50 # number of pub+sub pairs
# export OVERRIDE_RATE=500 # hz
# export OVERRIDE_QOS_RELIABILITY=reliable # system_default,reliable,best_effort,unknown
# export OVERRIDE_QOS_HISTORY=keep_last # system_default,keep_last,keep_all,unknown
# export OVERRIDE_QOS_DEPTH=10
# export OVERRIDE_LOG_DIR_POSTFIX='' # will be appended to the log directory name
# run 5



# # ----- change NUMBER (with best_effort) -----
# export OVERRIDE_NUMBER=1 # number of pub+sub pairs
# export OVERRIDE_RATE=200 # hz
# export OVERRIDE_QOS_RELIABILITY=best_effort # system_default,reliable,best_effort,unknown
# export OVERRIDE_QOS_HISTORY=keep_last # system_default,keep_last,keep_all,unknown
# export OVERRIDE_QOS_DEPTH=10
# export OVERRIDE_LOG_DIR_POSTFIX='' # will be appended to the log directory name
# run 5

# export OVERRIDE_NUMBER=5 # number of pub+sub pairs
# export OVERRIDE_RATE=200 # hz
# export OVERRIDE_QOS_RELIABILITY=best_effort # system_default,reliable,best_effort,unknown
# export OVERRIDE_QOS_HISTORY=keep_last # system_default,keep_last,keep_all,unknown
# export OVERRIDE_QOS_DEPTH=10
# export OVERRIDE_LOG_DIR_POSTFIX='' # will be appended to the log directory name
# run 5

# export OVERRIDE_NUMBER=25 # number of pub+sub pairs
# export OVERRIDE_RATE=200 # hz
# export OVERRIDE_QOS_RELIABILITY=best_effort # system_default,reliable,best_effort,unknown
# export OVERRIDE_QOS_HISTORY=keep_last # system_default,keep_last,keep_all,unknown
# export OVERRIDE_QOS_DEPTH=10
# export OVERRIDE_LOG_DIR_POSTFIX='' # will be appended to the log directory name
# run 5

# export OVERRIDE_NUMBER=50 # number of pub+sub pairs
# export OVERRIDE_RATE=200 # hz
# export OVERRIDE_QOS_RELIABILITY=best_effort # system_default,reliable,best_effort,unknown
# export OVERRIDE_QOS_HISTORY=keep_last # system_default,keep_last,keep_all,unknown
# export OVERRIDE_QOS_DEPTH=10
# export OVERRIDE_LOG_DIR_POSTFIX='' # will be appended to the log directory name
# run 5

