#!/bin/bash

# This is a script to test the internal bandwidth of the GPR network

# environment to setup (including all PC's IP and SSH account)

export AVPC_SSH_IP=192.168.88.4
export AVPC_SSH_PORT=22
export AVPC_SSH_USER=sanctuary
export AVPC_IPERF3_IP=192.168.88.4

export RTPC_SSH_IP=192.168.88.2
export RTPC_SSH_PORT=22
export RTPC_SSH_USER=administrator
export RTPC_IPERF3_IP=192.168.88.2

export CRBN_SSH_IP=192.168.88.136
export CRBN_SSH_PORT=22
export CRBN_SSH_USER=sanctuary
export CRBN_IPERF3_IP=192.168.88.136

export OUTPUT_PATH=~/Desktop/Netowrk-Status/output
# definations (no need to change)


TMUX_SESSION_NAME=gpr_bandwidth

TMUX_PANEL_AVPC_S=""
TMUX_PANEL_AVPC_C=""
TMUX_PANEL_RTPC_S=""
TMUX_PANEL_RTPC_C=""
TMUX_PANEL_CRBN_S=""
TMUX_PANEL_CRBN_C=""
TMUX_PANEL_HELP_1=""
TMUX_PANEL_HELP_2=""

IPERF_CLIENT_RUN_TIME=5

function test_ssh_connection() {
    # this is to test SSH connection to all specified PCs has been setup correctly on local PC
    echo "Test SSH to all PCs..."

    local connect_limit=5
    local overall_limit=$((connect_limit + 2))
    # ssh -o ConnectTimeout=5 user@ip_address -p port "pwd"
    # this will connect to the PC and print the current working directory in connection timeout of 5 seconds
    # in this case, the ssh key is not setup, so it will ask for password, wrap this command with "timeout" to avoid waiting for password input

    # test command success or not
    if timeout $overall_limit ssh -o ConnectTimeout=$connect_limit $AVPC_SSH_USER@$AVPC_SSH_IP -p $AVPC_SSH_PORT "pwd"; then
        echo "SSH to AVPC is OK"
    else
        echo "SSH to AVPC is failed"
        exit 1
    fi

    if timeout $overall_limit ssh -o ConnectTimeout=$connect_limit $RTPC_SSH_USER@$RTPC_SSH_IP -p $RTPC_SSH_PORT "pwd"; then
        echo "SSH to RTPC is OK"
    else
        echo "SSH to RTPC is failed"
        exit 1
    fi

    if timeout $overall_limit ssh -o ConnectTimeout=$connect_limit $CRBN_SSH_USER@$CRBN_SSH_IP -p $CRBN_SSH_PORT "pwd"; then
        echo "SSH to CRBN is OK"
    else
        echo "SSH to CRBN is failed"
        exit 1
    fi
}

function setup_tmux_panels() {
    echo "create new session"
    tmux new-session  -s $TMUX_SESSION_NAME -d

    # sleep 1

    # 1 => 2
    tmux split-window -h -t $TMUX_SESSION_NAME:0.0

    # 2 => 4
    tmux split-window -v -t $TMUX_SESSION_NAME:0.0
    tmux split-window -v -t $TMUX_SESSION_NAME:0.2

    # 4 => 8
    tmux split-window -v -t $TMUX_SESSION_NAME:0.0
    tmux split-window -v -t $TMUX_SESSION_NAME:0.2
    tmux split-window -v -t $TMUX_SESSION_NAME:0.4
    tmux split-window -v -t $TMUX_SESSION_NAME:0.6

    TMUX_PANEL_AVPC_S=$TMUX_SESSION_NAME:0.0
    TMUX_PANEL_AVPC_C=$TMUX_SESSION_NAME:0.4
    TMUX_PANEL_RTPC_S=$TMUX_SESSION_NAME:0.1
    TMUX_PANEL_RTPC_C=$TMUX_SESSION_NAME:0.5
    TMUX_PANEL_CRBN_S=$TMUX_SESSION_NAME:0.2
    TMUX_PANEL_CRBN_C=$TMUX_SESSION_NAME:0.6
    TMUX_PANEL_HELP_1=$TMUX_SESSION_NAME:0.3
    TMUX_PANEL_HELP_2=$TMUX_SESSION_NAME:0.7

}

function panel_send() {
    local panel=$1
    local command=$2

    echo "DEBUG: panel_send: ($panel) [$command]"

    tmux send-keys -t $panel "$command" C-m # && sleep 1
}

function command_in_ssh() {
    local ip=$1
    local port=$2
    local user=$3
    local command="${@:4}" # this is to get all the rest of the arguments
    # echo "command_in_ssh: [$command]"

    echo "ssh $user@$ip -p $port $command"
}

function create_kill_iperf3_command() {
    echo "echo '$(pgrep iperf3)' | xargs kill"
}

function create_iperf3_server_command() {
    local name=$1
    time=$(((IPERF_CLIENT_RUN_TIME*2+5)*3+10))
    echo "date | tee ~/iperf3_s_$name.txt ; timeout $time iperf3 -s | tee -a ~/iperf3_s_$name.txt"
}

function create_iperf3_client_command() {
    local name=$1
    local server_ip=$2
    local command="date | tee ~/iperf3_c_$name.txt ; iperf3 -c $server_ip -t 5 -u -b 900M  | tee -a ~/iperf3_c_$name.txt ; iperf3 -c $server_ip -t 5 -u -R -b 900M  | tee -a ~/iperf3_c_$name.txt"
    echo $command
}

function crreate_scp_command() {
    local ip=$1
    local port=$2
    local user=$3
    local file=$4
    local dest=$5

    # scp -P $AVPC_SSH_PORT $AVPC_SSH_USER@$AVPC_SSH_IP:~/iperf3_s_avpc.txt $OUTPUT_PATH/
    echo "scp -P $port $user@$ip:$file $dest"
}

# test_ssh_connection
# exit 0

if tmux has-session -t $TMUX_SESSION_NAME; then
    echo "session already exists"
    tmux kill-session -t $TMUX_SESSION_NAME
else
    setup_tmux_panels

    sleep 5

    # panel_send $TMUX_PANEL_HELP_1 "$(command_in_ssh $AVPC_SSH_IP $AVPC_SSH_PORT $AVPC_SSH_USER)"
    # panel_send $TMUX_PANEL_HELP_2 "$(command_in_ssh $RTPC_SSH_IP $RTPC_SSH_PORT $RTPC_SSH_USER)"

    # panel_send $TMUX_PANEL_AVPC_S "$(command_in_ssh $AVPC_SSH_IP $AVPC_SSH_PORT $AVPC_SSH_USER \"$(create_kill_iperf3_command))\" "
    # panel_send $TMUX_PANEL_RTPC_S "$(command_in_ssh $RTPC_SSH_IP $RTPC_SSH_PORT $RTPC_SSH_USER \"$(create_kill_iperf3_command))\" "
    # panel_send $TMUX_PANEL_CRBN_S "$(command_in_ssh $CRBN_SSH_IP $CRBN_SSH_PORT $CRBN_SSH_USER \"$(create_kill_iperf3_command))\" "
    # sleep 2

    panel_send $TMUX_PANEL_AVPC_S "$(command_in_ssh $AVPC_SSH_IP $AVPC_SSH_PORT $AVPC_SSH_USER \"$(create_iperf3_server_command avpc))\" "
    panel_send $TMUX_PANEL_RTPC_S "$(command_in_ssh $RTPC_SSH_IP $RTPC_SSH_PORT $RTPC_SSH_USER \"$(create_iperf3_server_command rtpc))\" "
    panel_send $TMUX_PANEL_CRBN_S "$(command_in_ssh $CRBN_SSH_IP $CRBN_SSH_PORT $CRBN_SSH_USER \"$(create_iperf3_server_command carbon))\" "
    sleep 2

    # carbon - rtpc
    panel_send $TMUX_PANEL_CRBN_C "$(command_in_ssh $CRBN_SSH_IP $CRBN_SSH_PORT $CRBN_SSH_USER \"$(create_iperf3_client_command carbon $AVPC_IPERF3_IP))\" "
    sleep $((IPERF_CLIENT_RUN_TIME*2 + 5))

    # avpc - rtpc
    panel_send $TMUX_PANEL_AVPC_C "$(command_in_ssh $AVPC_SSH_IP $AVPC_SSH_PORT $AVPC_SSH_USER \"$(create_iperf3_client_command avpc $RTPC_IPERF3_IP))\" "
    sleep $((IPERF_CLIENT_RUN_TIME*2 + 5))

    # rtpc - carbon
    panel_send $TMUX_PANEL_RTPC_C "$(command_in_ssh $RTPC_SSH_IP $RTPC_SSH_PORT $RTPC_SSH_USER \"$(create_iperf3_client_command rtpc $CRBN_IPERF3_IP))\" "
    sleep $((IPERF_CLIENT_RUN_TIME*2 + 5))

    sleep 20

    # gather all the output files
    panel_send $TMUX_PANEL_HELP_1 "rm -rf $OUTPUT_PATH/; mkdir -p $OUTPUT_PATH"

    panel_send $TMUX_PANEL_HELP_1 "$(crreate_scp_command $AVPC_SSH_IP $AVPC_SSH_PORT $AVPC_SSH_USER '~/iperf3_s_avpc.txt' $OUTPUT_PATH/)" # log for iperf3 server from avpc
    panel_send $TMUX_PANEL_HELP_1 "$(crreate_scp_command $AVPC_SSH_IP $AVPC_SSH_PORT $AVPC_SSH_USER '~/iperf3_c_avpc.txt' $OUTPUT_PATH/)" # log for iperf3 client from avpc

    panel_send $TMUX_PANEL_HELP_1 "$(crreate_scp_command $RTPC_SSH_IP $RTPC_SSH_PORT $RTPC_SSH_USER '~/iperf3_s_rtpc.txt' $OUTPUT_PATH/)" # log for iperf3 server from rtpc
    panel_send $TMUX_PANEL_HELP_1 "$(crreate_scp_command $RTPC_SSH_IP $RTPC_SSH_PORT $RTPC_SSH_USER '~/iperf3_c_rtpc.txt' $OUTPUT_PATH/)" # log for iperf3 client from rtpc

    panel_send $TMUX_PANEL_HELP_1 "$(crreate_scp_command $CRBN_SSH_IP $CRBN_SSH_PORT $CRBN_SSH_USER '~/iperf3_s_carbon.txt' $OUTPUT_PATH/)" # log for iperf3 server from carbon
    panel_send $TMUX_PANEL_HELP_1 "$(crreate_scp_command $CRBN_SSH_IP $CRBN_SSH_PORT $CRBN_SSH_USER '~/iperf3_c_carbon.txt' $OUTPUT_PATH/)" # log for iperf3 client from carbon

fi

echo "--- DONE ---"
