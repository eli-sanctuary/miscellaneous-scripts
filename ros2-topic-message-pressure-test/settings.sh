#!/bin/bash

export NUMBER=10 # number of pub+sub pairs
export RATE=1000 # hz


export QOS_RELIABILITY=reliable  # system_default,reliable,best_effort,unknown
export QOS_HISTORY=keep_last # system_default,keep_last,keep_all,unknown
export QOS_DEPTH=10

# --qos-depth $QOS_DEPTH --qos-history $QOS_HISTORY --qos-reliability $QOS_RELIABILITY

export OVERLAP_TIME=20 # in seconds

export LOG_DIR_POSTFIX='' # will be appended to the log directory name
