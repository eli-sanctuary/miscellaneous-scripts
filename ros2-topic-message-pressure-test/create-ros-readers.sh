#!/bin/bash

# Get the absolute path of the directory containing the script
script_dir="$( cd "$( dirname "${BASH_SOURCE[0]}" )" &> /dev/null && pwd )"

# Source the file using its absolute path
source "$script_dir/settings.sh"
source "$script_dir/common.sh"


# ------------------------------

# Steps to execute the script:
# 1. ros2 topic list              (sync)
WAIT_BEFORE_TOPIC_LIST=1

# 2. ros2 topic echo /topic/test* (async + timeout)
WAIT_BEFORE_TOPIC_ECHO=1
ECHO_TIME=$(( $NUMBER + $OVERLAP_TIME  + $KEEP_PUB_NODE_ALIVE + 10))

# 3. ros2 topic info /topic/test* (sync)
WAIT_BEFORE_TOPIC_INFO=10

# 4. ros2 bag record              (sync + timeout)
WAIT_BEFORE_BAG_RECORD=5
BAG_RECORD_TIME=0

# Create a directory for the subscriber log files
if [ ! -d $LOG_DIR/sub ]; then
    mkdir -p $LOG_DIR/sub
fi

# Create a directory for the subscriber log files
if [ ! -d $LOG_DIR/info ]; then
    mkdir -p $LOG_DIR/info
fi

# Create a directory for the RTI log files
if [ ! -d $LOG_DIR/rti_log ]; then
    mkdir -p $LOG_DIR/rti_log
fi

# Remove old log files
rm $LOG_DIR/sub/log_echo_*.txt > /dev/null 2>&1
rm $LOG_DIR/info/log_info_*.txt > /dev/null 2>&1

# Create a directory for the ros2 cli output as log files
if [ ! -d ~/rosbag2_output ]; then
    mkdir -p ~/rosbag2_output
fi

tcpdump -i any -w $LOG_DIR/reader.pcap &

echo_topic() {
  local output_file=$LOG_DIR/sub/log_echo_$i.txt
  # output_file=~/logs/temp.txt

  # e.g.
  # ros2 topic echo /topic/test1 "std_msgs/String" --full-length --qos-depth $QOS_DEPTH --qos-history $QOS_HISTORY --qos-reliability $QOS_RELIABILITY

  echo_with_time "Start echo /topic/test$i to $output_file [$ECHO_TIME seconds]"

  echo "" > $output_file

  # Note: having trouble in redirecting the output of ros2 topic echo to a file
  # timeout --preserve-status --foreground --verbose --kill-after=20s $ECHO_TIME ros2 topic echo /topic/test$i $MESSAGE_TYPE --full-length --qos-depth $QOS_DEPTH --qos-history $QOS_HISTORY --qos-reliability $QOS_RELIABILITY >> $output_file
  # ros2 topic echo /topic/test$i $MESSAGE_TYPE --full-length --qos-depth $QOS_DEPTH --qos-history $QOS_HISTORY --qos-reliability $QOS_RELIABILITY >> $output_file &
  # stdbuf -o0 ros2 topic echo /topic/test$i $MESSAGE_TYPE --full-length --qos-depth $QOS_DEPTH --qos-history $QOS_HISTORY --qos-reliability $QOS_RELIABILITY >> $output_file &
  # stdbuf -o0 ros2 topic echo /topic/test$i $MESSAGE_TYPE --full-length --qos-depth $QOS_DEPTH --qos-history $QOS_HISTORY --qos-reliability $QOS_RELIABILITY | stdbuf -o0  tee $output_file &

  script -c "LOG_FILE_ID=reader-echo-$i ros2 topic echo /topic/test$i $MESSAGE_TYPE --full-length --qos-depth $QOS_DEPTH --qos-history $QOS_HISTORY --qos-reliability $QOS_RELIABILITY" -q -f $output_file > /dev/null &
  # timeout --preserve-status --foreground --verbose --kill-after=20s $ECHO_TIME script -c "ros2 topic echo /topic/test$i $MESSAGE_TYPE --full-length --qos-depth $QOS_DEPTH --qos-history $QOS_HISTORY --qos-reliability $QOS_RELIABILITY" -q -f $output_file
  
  # Get the PID of the command
  command_pid=$!
  sleep $ECHO_TIME

  # Kill the command
  kill -SIGINT $command_pid

  echo_with_time "Finished echo /topic/test$i [$output_file]"
  
  # sync

  # Count the number of lines containing "---" in a file
  count=$(grep -c "\-\-\-" $output_file)

  echo -e "echo from /topic/test$i \t: $count" >> $LOG_DIR/reader-count.txt
  echo_with_time "Reader $i Count: $count"
}

start_all_topic_echo() {
  if [ $ECHO_TIME -gt 0 ]; then
    if [ $WAIT_BEFORE_TOPIC_ECHO -gt 0 ]; then
      echo_with_time "Wait for $WAIT_BEFORE_TOPIC_ECHO seconds, to run: ros2 topic echo"
      sleep $WAIT_BEFORE_TOPIC_ECHO
    fi

    echo_with_time "Start running ros2 topic echo, individually asynchronously"
    for ((i=1; i<=$NUMBER; i++))
    do
      # add a delay to avoid all the publishers start at the same time
      sleep 1

      echo_with_time "Start echo /topic/test$i"
      echo_topic &
    done

    echo "Background jobs: "
    echo "$(jobs -p | tr '\n' ' ')"
  fi
}

start_all_topic_info() {
  echo_with_time "Start running ros2 topic info individually one by one"
  for ((i=1; i<=$NUMBER; i++))
  do
    sleep 1
    LOG_FILE_ID=reader-info-$i ros2 topic info /topic/test$i --verbose > $LOG_DIR/info/log_info_$i.txt
  done
}

detect_message_lost_in_file() {
  local output_file=$1
  local result_file=$2

  local min=1
  local max=$(( ($NUMBER + $OVERLAP_TIME ) * $RATE))

  # lost=$(detect_msg_loss $output_file $min $max)
  lost=$(python3 "$script_dir/detect_msg_lost.py" $output_file $min $max)  

  echo_with_time "Detecting lost messages in $output_file finished"

  # Open file descriptor 200 for read/write
  exec 200<> $result_file

  # enter Critical section...
  flock -x 200

    echo $output_file [ $min ... $max ]  >> $result_file
    format_continuous_numbers $lost      >> $result_file
    echo -e " \n"                        >> $result_file

  # leave Critical section...
  flock -u 200

  # Close file descriptor
  exec 200>&-

  echo_with_time "Detecting lost messages in $output_file saved"
}


# Step 1: ros2 topic list
if [ $WAIT_BEFORE_TOPIC_LIST -gt 0 ]; then
  echo_with_time "Wait for $WAIT_BEFORE_TOPIC_LIST seconds, to run: ros2 topic list"
  sleep $WAIT_BEFORE_TOPIC_LIST
fi

echo_with_time "Start running ros2 topic list"
start=$(date +%s)
LOG_FILE_ID=reader-list ros2 topic list
end=$(date +%s)
echo_with_time "Finished running ros2 topic list in $((end - start)) seconds"


# Step 2: ros2 topic echo /topic/test*
start_all_topic_echo &


# Step 3: ros2 topic info /topic/test*
if [ $WAIT_BEFORE_TOPIC_INFO -gt 0 ]; then
  echo_with_time "Wait for $WAIT_BEFORE_TOPIC_INFO seconds, to run: ros2 topic info"
  sleep $WAIT_BEFORE_TOPIC_INFO
fi

start_all_topic_info &


# Step 4: ros2 bag record
if [ $BAG_RECORD_TIME -gt 0 ]; then
  if [ $WAIT_BEFORE_BAG_RECORD -gt 0 ]; then
    echo_with_time "Wait for $WAIT_BEFORE_BAG_RECORD seconds, to run: ros2 bag record"
    sleep $WAIT_BEFORE_BAG_RECORD
  fi

  echo_with_time "Start ros2 bag record for $BAG_RECORD_TIME seconds"
  all_topics=""
  for ((i=1; i<=$NUMBER; i++))
  do
    all_topics="$all_topics /topic/test$i"
  done
  cd $LOG_DIR
  output_name="ros2bag $RMW_IMPLEMENTATION $NUMBER x $RATE $NOW"
  output_name="${output_name// /_}" # replace all spaces with _
  echo_with_time "output file: $output_name"
  timeout --preserve-status --foreground --verbose --kill-after=20s $BAG_RECORD_TIME ros2 bag record --output $output_name --storage mcap $all_topics

  echo_with_time "Finished"
fi

# echo "Press any key to stop subscribing."
# read -n 1 -s -r

# wait for all background jobs to finish by calculated time
sleep $ECHO_TIME
sleep $NUMBER
sleep 10

echo_with_time "Stop all background reader jobs:"
echo "$(jobs -p | tr '\n' ' ')"
for job in `jobs -p`
do
  kill -SIGINT $job
done

# # wait for all background jobs to finish
# wait
# echo_with_time "All background reader jobs have finished."
# sleep 1

env | sort > $LOG_DIR/reader_env.txt
LOG_FILE_ID=reader-doctor ros2 doctor --report  > $LOG_DIR/sub/ros2-doctor.txt

read_numbers=($(parse_numbers $LOG_DIR/reader-count.txt))
read_total=$(sum "${read_numbers[@]}")
write_numbers=($(parse_numbers $LOG_DIR/writer-count.txt))
write_total=$(sum "${write_numbers[@]}")
lost_rate=$(echo "scale=6; ($write_total - $read_total) / $write_total" | bc)

echo ""                              >> $LOG_DIR/reader-count.txt
echo "----------"                    >> $LOG_DIR/reader-count.txt
echo "Total Received : $read_total"  >> $LOG_DIR/reader-count.txt
echo "Total Published: $write_total" >> $LOG_DIR/reader-count.txt
echo "Lost Rate      : $lost_rate"   >> $LOG_DIR/reader-count.txt

if [ $write_total -gt $read_total ]; then
  # there is message lost
  echo_with_time "Detecting lost messages"
  for ((i=1; i<=$NUMBER; i++))
  do
    output_file=$LOG_DIR/sub/log_echo_$i.txt
    echo_with_time "Detecting lost messages in $output_file started"

    detect_message_lost_in_file $output_file $LOG_DIR/reader-lost.txt &
  done

  # wait for all background jobs to finish
  wait
else
  echo "No lost messages detected" > $LOG_DIR/reader-no-lost.txt
fi

ipcs -a > $LOG_DIR/reader-ipcs.txt

echo_with_time "Exiting Reader"