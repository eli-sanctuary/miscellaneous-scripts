# Test Message Lost with ROS2 CLI Tools

Using two docker containers both based on carbon-core image.
Using one of them to run multiple “ros2 topic pub ... “,
Another to run multiple “ros2 topic echo ...“
Meanwhile, using “ros2 bag record“ or data capture agent to record messages

## How to use?

Run create-ros-writers.sh in one container first.
Then run create-ros-readers.sh in another container.
data capture agent is not included in this repository.

## How build the docker image?

```bash
docker build --target ros2-foxy-pure --tag ros2-foxy-ubuntu20.04 .
docker build --target ros2-foxy-rti  --tag ros2-foxy-ubuntu20.04-rti .
```

### Create a docker container to run script in it (works for both writer and reader)

```bash
# using default QoS
docker run  -it \
            --rm \
            --ipc=host \
            --shm-size='10gb' \
            --network=host \
            --name temp_$(date '+%Y-%m-%d_%H-%M-%S') \
            -e ROS_DOMAIN_ID=230 \
            -e RTI_LICENSE_FILE=/root/rti/license.dat \
            -v ~/code/connextdds_qos_profiles/license/RTI_ConnextDDS_License.dat:/root/rti/license.dat \
            -v ~/code/eli-sanctuary/miscellaneous-scripts/ros2-topic-message-pressure-test:/root/scripts \
            -v ~/Downloads/logs:/root/logs \
            ros2-foxy-ubuntu20.04-rti
```

```bash
# using SAI QoS XML files
docker run  -it \
            --rm \
            --ipc=host \
            --shm-size='10gb' \
            --network=host \
            --name temp_$(date '+%Y-%m-%d_%H-%M-%S') \
            -e ROS_DOMAIN_ID=230 \
            -e RTI_LICENSE_FILE=/root/rti/license.dat \
            -e NDDS_DISCOVERY_PEERS=50@localhost,50@shmem:// \
            -e NDDS_QOS_PROFILES="/root/rti/qos_profiles/profiles/network/unicast_qos.xml;/root/rti/qos_profiles/profiles/base/sanctuary_base_profile.xml;/root/rti/qos_profiles/profiles/applications/carbon.xml" \
            -v ~/code/connextdds_qos_profiles/license/RTI_ConnextDDS_License.dat:/root/rti/license.dat \
            -v ~/code/connextdds_qos_profiles:/root/rti/qos_profiles \
            -v ~/code/eli-sanctuary/miscellaneous-scripts/ros2-topic-message-pressure-test:/root/scripts \
            -v ~/Downloads/logs:/root/logs \
            ros2-foxy-ubuntu20.04-rti
```

### build ROS2 workspace (in container), to make the message contains id to detect lost

```bash
cd ~/scripts/ros2_ws/
colcon build --symlink-install --allow-overriding ros2action ros2cli ros2component ros2doctor ros2interface ros2lifecycle ros2multicast ros2node ros2param ros2pkg ros2run ros2service ros2topic

```

### Launch writer and reader

```bash
~/scripts/create-ros-writers.sh
~/scripts/create-ros-readers.sh
```

### Run both writer and reader by docker-compose

```bash
clear && docker compose up   && docker compose down 
clear && docker compose down && docker compose up 
```

## Export python script to generate message-lost statistics report from log files

```bash
cd ~/code/eli-sanctuary/miscellaneous-scripts/ros2-topic-message-pressure-test
python3 message-lost-statistic.py ~/Downloads/logs ~/Downloads/logs/table.csv ~/Downloads/logs/matrix-gap.csv ~/Downloads/logs/matrix-cpu.csv
```
