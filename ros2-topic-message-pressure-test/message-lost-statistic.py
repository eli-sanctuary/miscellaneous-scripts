"""This module contains classes and functions for processing and analyzing message loss statistics."""
import csv
import fnmatch
import os
import re
import sys

# define constant variables
LOST_REPORT_FILE = "reader-lost.txt"
NO_LOST_REPORT_FILE = "reader-no-lost.txt"
READER_COUNT_FILE = "reader-count.txt"
WRITER_COUNT_FILE = "writer-count.txt"
ENV_FILE = "writer_env.txt"
SAR_FILE = "sar.txt"

CRITICAL_GAP_LENGTH = 10  # in ms


class MessageLostStatistic:
    """This class represents the message lost statistics."""

    def __init__(self):
        self.log_path = None
        self.pairs = 0
        self.rate = 0
        self.qos_reliability = None
        self.qos_history = None
        self.qos_depth = 0
        self.number_of_gap = 0
        self.number_of_critical_gap = 0
        self.number_of_beginning_part_missing = 0
        self.number_of_complete_missing = 0
        self.max_gap_length = 0
        self.average_gap_length = 0
        self.median_gap_length = 0
        self.overall_lost_rate = 0
        self.cpu_average = 0
        self.cpu_max = 0

    def __str__(self):
        return f"MessageLostStatistic: {self.pairs}, {self.rate}, {self.qos_reliability}, {self.qos_history}, {self.qos_depth} Log: {self.log_path}"


class ReaderCountInfo:
    """This class represents the count information of a reader."""

    def __init__(self):
        self.total_received = 0
        self.total_published = 0
        self.lost_rate = 0

    def __str__(self):
        return f"RedderCount: received {self.total_received} from {self.total_published}, lost: {self.lost_rate}"


# def find_matching_files(folder, pattern):
#     matching_files = []
#     for root, dirs, files in os.walk(folder):
#         for filename in files:
#             if fnmatch.fnmatch(filename, pattern):
#                 matching_files.append(os.path.join(root, filename))
#     return matching_files


def find_matching_dirs(folder, pattern):
    matching_dirs = []
    for root, dirs, files in os.walk(folder):
        for filename in files:
            if fnmatch.fnmatch(filename, pattern):
                folder = os.path.dirname(os.path.join(root, filename))
                matching_dirs.append(folder)
    return matching_dirs


def find_median(arr):
    # Sort the array
    sorted_arr = sorted(arr)

    # Find the length of the array
    n = len(sorted_arr)

    # Check if the length is odd or even
    if n % 2 == 1:
        # If the length is odd, return the middle element
        median = sorted_arr[n // 2]
    else:
        # If the length is even, return the average of the two middle elements
        median = (sorted_arr[n // 2 - 1] + sorted_arr[n // 2]) / 2

    return median


def calculate_average(arr):
    # Check if the array is empty
    if not arr:
        return None

    # Calculate the sum of all elements in the array
    total = sum(arr)

    # Calculate the average
    average = total / len(arr)

    return average


def split_by_non_numbers(input_string):
    # Use regular expression to split the string by any non-number characters
    parts = re.split(r'\D+', input_string)
    numbers = []
    for part in parts:
        if part:
            numbers.append(int(part))
    return numbers


def check_files_exist(folder, files):
    for file in files:
        if not os.path.exists(os.path.join(folder, file)):
            return False
    return True


def parse_env_file(file, result=None):
    if result is None:
        result = MessageLostStatistic()

    with open(file, "r") as f:
        lines = f.readlines()

    for line in lines:
        ln = line.strip()
        if ln.startswith("NUMBER"):
            result.pairs = int(ln.split("=")[1].strip())
        elif ln.startswith("RATE"):
            result.rate = int(ln.split("=")[1].strip())
        elif ln.startswith("QOS_RELIABILITY"):
            result.qos_reliability = ln.split("=")[1].strip()
        elif ln.startswith("QOS_HISTORY"):
            result.qos_history = ln.split("=")[1].strip()
        elif ln.startswith("QOS_DEPTH"):
            result.qos_depth = int(ln.split("=")[1].strip())
        else:
            continue

    return result


def parse_log_path(path, result=None):
    if result is None:
        result = MessageLostStatistic()
        # result.qos_reliability = None
        # result.qos_history = None
        # result.qos_depth = 0
        # result.pairs = 0
        # result.rate = 0

    result.log_path = path  # get containing folder name

    # path example: '2024-04-03_06-18-25-[10_200]-reliable-keep_all-100'

    if "reliable" in path:
        result.qos_reliability = "reliable"
    elif "best_effort" in path:
        result.qos_reliability = "best_effort"

    if "keep_all" in path:
        result.qos_history = "keep_all"
    elif "keep_last" in path:
        result.qos_history = "keep_last"

    info = path.split("[")[1]  # get substring after the first '['
    numbers = split_by_non_numbers(info)
    if len(numbers) == 3:
        result.pairs = numbers[0]
        result.rate = numbers[1]
        result.qos_depth = numbers[2]
    else:
        print(f"Error: Invalid numbers. {numbers} in path [{path}]")
        # raise Exception("Invalid numbers")
        # return None

    return result


def parse_sar_file(file):
    try:
        # Open the sar file
        with open(file, "r") as file:
            lines = file.readlines()

        take = False
        idle_values = []

        # Parse the lines starting from the line with %idle
        cpu_idle_percentage = None
        for a_line in lines:
            line = a_line.strip()
            if line.find("%idle") != -1:
                take = True
                continue  # skip this line

            # if this line is empty, skip it
            if len(line) == 0:
                take = False
                continue

            if take:
                # Split the line by whitespace
                parts = line.split()
                # Get the CPU idle percentage
                cpu_idle_percentage = float(parts[-1]) / 100
                if (cpu_idle_percentage >= 0):
                    idle_values.append(1 - cpu_idle_percentage)

        average = sum(idle_values) / len(idle_values)
        maximum = max(idle_values)

        return average, maximum
    except Exception as e:
        # raise e
        print(f"Error: {e}")
        return -1, -1
    # end try


def get_all_ids(ln):
    # print(f' - : {ln}')
    all_ids = ln.split(" ")

    # check each a_id in the list is valid and continuous increasing by 1
    pre_id = 0
    for a_id in all_ids:
        int_id = int(a_id)
        if int_id <= 1 and (pre_id + 1) != int_id:
            # this is not right, throw an exception
            raise Exception("Invalid ID")
        pre_id = int_id
    return all_ids


def parse_lost_report_file(file, result=None):
    gap_at_beginning_counter = 0
    gap_sizes = []
    last_max = 0
    number_of_complete_missing = 0
    number_of_beginning_part_missing = 0

    try:
        # try to read the file
        with open(file, "r") as f:
            lines = f.readlines()

        # parse the content
        for line in lines:
            ln = line.strip()
            if len(ln) == 0:  # this line is empty
                continue
            elif ln.startswith("/"):  # this line is a path of log file
                # Use regular expression to extract the desired part
                match = re.search(r'\[ (\d+) \.\.\. (\d+) \]', ln)
                if match:
                    start_number = int(match.group(1))
                    end_number = int(match.group(2))
                    if end_number > start_number:
                        last_max = end_number
                continue
            elif ln.startswith("1 "):  # this line is a gap at the beginning
                gap_at_beginning_counter += 1
                all_ids = get_all_ids(ln)

                # check if the last id is the max id
                if last_max > 0 and int(all_ids[-1]) == last_max:
                    number_of_complete_missing += 1
                else:
                    number_of_beginning_part_missing += 1
                continue
            else:  # this is a gap
                all_ids = get_all_ids(ln)
                gap_sizes.append(len(all_ids))

                continue
    except FileNotFoundError:
        print(f"Error: File not found. {file}")
    except Exception as e:
        raise e
    # end try

    result.number_of_gap = len(gap_sizes)

    if CRITICAL_GAP_LENGTH > 0 and result.rate > 0:
        ms_per_gap = (1 / result.rate) * 1000
        critical_gap_length = int(CRITICAL_GAP_LENGTH / ms_per_gap)
        result.number_of_critical_gap = len([x for x in gap_sizes if x > critical_gap_length])

    if result.number_of_gap > 0:
        result.max_gap_length = max(gap_sizes)
        result.average_gap_length = calculate_average(gap_sizes)
        result.median_gap_length = find_median(gap_sizes)
        result.number_of_beginning_part_missing = number_of_beginning_part_missing
        result.number_of_complete_missing = number_of_complete_missing

    return result


def parse_total_count_report_file(file):
    with open(file, "r") as f:
        lines = f.readlines()

    info = ReaderCountInfo()
    for line in lines:
        try:
            if "Total Received" in line:
                info.total_received = int(line.split(":")[1].strip())
            elif "Total Published" in line:
                info.total_published = int(line.split(":")[1].strip())
            elif "Lost Rate" in line:
                info.lost_rate = float(line.split(":")[1].strip())
            else:
                pass
        except Exception as e:
            print(f"Error: Invalid line. {line} in file {file}")
            print(e)
            continue

    if info.total_received > 0 and info.total_published > 0:
        return info
    else:
        return None


def display_gap_length(gap_len, rate, decimal=2):
    circle = 1000 / rate  # in ms
    time_r = round(circle * gap_len, decimal + 2)
    gap_len_r = round(gap_len, decimal + 2)

    def format_to_string(float_number):
        template = f"{{:,.{decimal}f}}"
        formatted_number = template.format(float(float_number))
        return formatted_number

    if gap_len > 0:
        text = f"{format_to_string(gap_len_r)} [{format_to_string(time_r)} ms]"
        return text
    else:
        return ""


def display_in_percentage(value):
    return "" if value == 0 else "{:.2%}".format(value)


def write_statistic_to_table(filename, statistic):
    data = [
        ["Pairs", "Rate", "QoS", "Lost Rate",
         "Beginning Lost", "Complete Lost",
         "Number of Gap", "Max Gap Length", "Average Gap Length", "Median Gap Length",
         f"Number of Gap >{CRITICAL_GAP_LENGTH}ms",
         "CPU Average", "CPU Max", "Log Path"]
    ]
    for item in statistic:
        qos_str = f"{item.qos_reliability}"
        if item.qos_reliability == "reliable":
            qos_str += f" - {item.qos_history}"
            if item.qos_history == "keep_last":
                qos_str += f" - {item.qos_depth}"

        data.append([item.pairs, item.rate,
                     qos_str,
                     display_in_percentage(item.overall_lost_rate),
                     item.number_of_beginning_part_missing if item.number_of_beginning_part_missing > 0 else "",
                     item.number_of_complete_missing if item.number_of_complete_missing > 0 else "",
                     item.number_of_gap,
                     display_gap_length(item.max_gap_length, item.rate, 0),
                     display_gap_length(item.average_gap_length, item.rate, 2),
                     display_gap_length(item.median_gap_length, item.rate, 0),
                     item.number_of_critical_gap if item.number_of_critical_gap > 0 else "",
                     display_in_percentage(item.cpu_average), display_in_percentage(item.cpu_max),
                     item.log_path])

    with open(filename, 'w', newline='') as csv_file:
        csv_writer = csv.writer(csv_file)
        csv_writer.writerows(data)


def write_statistic_to_matrix(filename, statistic, field_name):
    x_max = []  # pairs
    y_max = []  # rate
    for item in statistic:
        if item.pairs not in x_max:
            x_max.append(item.pairs)
        if item.rate not in y_max:
            y_max.append(item.rate)

    x_max.sort()
    y_max.sort()

    matrix = [[None for _ in range(len(x_max))] for _ in range(len(y_max))]

    for item in statistic:
        x_index = x_max.index(item.pairs)
        y_index = y_max.index(item.rate)
        if matrix[y_index][x_index] is None:
            matrix[y_index][x_index] = item

    data = [
        # ["Rate \\ Pairs"] + x_max
        ["Rate \\ Pairs"] + [f"Pairs of {x}" for x in x_max]
    ]

    for line in matrix:
        # row = [y_max[matrix.index(line)]]
        row = [f"Rate {y_max[matrix.index(line)]}"]
        for item in line:
            if item is None:
                row.append("")
            else:
                # row.append(item.number_of_critical_gap)
                row.append(getattr(item, field_name, ""))
        data.append(row)

    with open(filename, 'w', newline='') as csv_file:
        csv_writer = csv.writer(csv_file)
        csv_writer.writerows(data)


def main():
    if len(sys.argv) < 3:
        print("Usage: python message-lost-statistic.py <folder_path> <output_table> [<field_name> <output_matrix>]")
        sys.exit(1)

    folder = sys.argv[1]
    output_table = sys.argv[2]
    # output_matrix_gap = sys.argv[3]
    # output_matrix_cpu = sys.argv[4]

    if not os.path.isdir(folder):
        print("Error: Invalid folder path.")
        sys.exit(1)

    all_statistic = []

    matching_dirs = find_matching_dirs(folder, LOST_REPORT_FILE)
    matching_dirs.extend(find_matching_dirs(folder, NO_LOST_REPORT_FILE))

    if matching_dirs:
        # print("Matching files found:")
        for folder in sorted(matching_dirs):
            if check_files_exist(folder, [READER_COUNT_FILE, WRITER_COUNT_FILE, ENV_FILE]):
                print(folder)
                count_report = os.path.join(folder, READER_COUNT_FILE)
                reader_count_info = parse_total_count_report_file(count_report)

                if reader_count_info is not None:
                    # print(f"( {reader_count_info} )")
                    message_lost_statistic = MessageLostStatistic()

                    base_dir = os.path.basename(folder)
                    message_lost_statistic.log_path = base_dir
                    # parse_log_path(base_dir, message_lost_statistic)

                    env_file = os.path.join(folder, ENV_FILE)
                    parse_env_file(env_file, message_lost_statistic)

                    lost_report_file = os.path.join(folder, LOST_REPORT_FILE)
                    parse_lost_report_file(lost_report_file, message_lost_statistic)

                    sar_file = os.path.join(folder, SAR_FILE)
                    average, maximum = parse_sar_file(sar_file)

                    message_lost_statistic.overall_lost_rate = reader_count_info.lost_rate

                    message_lost_statistic.cpu_average = average
                    message_lost_statistic.cpu_max = maximum

                    all_statistic.append(message_lost_statistic)
                    print(f"[ {message_lost_statistic} ]")
                else:
                    print(f"Error: Invalid reader count info in file {count_report}")
                    continue
            else:
                print(f"Error: Missing files in folder {folder}")
                continue

    else:
        print("No matching files found.")
        sys.exit(1)

    # output
    if output_table:
        print("Output Table to  CSV file:", output_table)
        write_statistic_to_table(output_table, all_statistic)

    for i in range(3, len(sys.argv), 2):
        if (len(sys.argv) >= i + 1):
            field_name = sys.argv[i]
            output_matrix = sys.argv[i+1]
            print(f"Output Matrix on {field_name} to CSV file: {output_matrix}")
            write_statistic_to_matrix(output_matrix, all_statistic, field_name)

    # if output_matrix_gap:
    #     print("Output GAP Matrix to  CSV file:", output_matrix_gap)
    #     write_statistic_to_matrix(output_matrix_gap, all_statistic, "number_of_critical_gap")

    # if output_matrix_cpu:
    #     print("Output CPU Matrix to  CSV file:", output_matrix_cpu)
    #     write_statistic_to_matrix(output_matrix_cpu, all_statistic, "cpu_max")


if __name__ == "__main__":
    main()
