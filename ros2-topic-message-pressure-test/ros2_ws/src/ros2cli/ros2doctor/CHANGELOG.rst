^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^
Changelog for package ros2doctor
^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^

0.9.13 (2023-03-16)
-------------------
* Added changelogs
* Contributors: Dharini Dutia

0.9.12 (2022-09-12)
-------------------

0.9.11 (2022-01-31)
-------------------

0.9.10 (2021-10-05)
-------------------

0.9.9 (2021-03-24)
------------------
* 0.9.9
* Contributors: Audrow Nash

0.9.8 (2020-12-08)
------------------

0.9.7 (2020-07-07)
------------------

0.9.6 (2020-06-23)
------------------

0.9.5 (2020-06-01)
------------------

0.9.4 (2020-05-26)
------------------
* Ensure ros2doctor ROS nodes have valid names. (`#513 <https://github.com/ros2/ros2cli/issues/513>`_)
* Contributors: Michel Hidalgo

0.9.3 (2020-05-13)
------------------

0.9.2 (2020-05-08)
------------------

0.9.1 (2020-05-06)
------------------

0.9.0 (2020-04-29)
------------------
* Make sure to add ros2doctor verbs to the extension points. (`#495 <https://github.com/ros2/ros2cli/issues/495>`_)
* [ros2doctor] Only report topic warnings if publisher or subscription count is zero (`#472 <https://github.com/ros2/ros2cli/issues/472>`_)
* more verbose test_flake8 error messages (same as `ros2/launch_ros#135 <https://github.com/ros2/launch_ros/issues/135>`_)
* Prefix ros2doctor node names with 'ros2doctor\_' (`#451 <https://github.com/ros2/ros2cli/issues/451>`_)
* [ros2doctor] Handle non-metapackages in rosdistro check (`#452 <https://github.com/ros2/ros2cli/issues/452>`_)
* [ros2doctor] Improve doctor_warn()  (`#445 <https://github.com/ros2/ros2cli/issues/445>`_)
* Multimachine communication (`#432 <https://github.com/ros2/ros2cli/issues/432>`_)
* [ros2doctor] Check for deprecated packages (`#388 <https://github.com/ros2/ros2cli/issues/388>`_)
* Revert "simplify helper function"
* simplify helper function
* Update network check to fix missing flags on Windows  (`#404 <https://github.com/ros2/ros2cli/issues/404>`_)
* Contributors: Chris Lalancette, Claire Wang, Dirk Thomas, Jacob Perron, claireyywang

0.8.6 (2019-11-19)
------------------

0.8.5 (2019-11-14)
------------------
* 0.8.5
* Contributors: Shane Loretz

0.8.4 (2019-11-13)
------------------
* 0.8.4
* update headline to capital letters (`#383 <https://github.com/ros2/ros2cli/issues/383>`_)
* Contributors: Claire Wang, Michael Carroll

0.8.3 (2019-10-23)
------------------
* 0.8.3
* Update failed modules message (`#380 <https://github.com/ros2/ros2cli/issues/380>`_)
* fix AttributeError (`#370 <https://github.com/ros2/ros2cli/issues/370>`_)
* add new args (`#354 <https://github.com/ros2/ros2cli/issues/354>`_)
* Contributors: Claire Wang, Marya Belanger, Shane Loretz

0.8.2 (2019-10-08)
------------------
* 0.8.2
* Contributors: Dirk Thomas

0.8.1 (2019-10-04)
------------------
* 0.8.1
* ros2doctor: add topic check (`#341 <https://github.com/ros2/ros2cli/issues/341>`_)
* Contributors: Claire Wang, Michael Carroll

0.8.0 (2019-09-26)
------------------
* install resource marker file for packages (`#339 <https://github.com/ros2/ros2cli/issues/339>`_)
* ros2doctor: add `--include-warning` arg (`#338 <https://github.com/ros2/ros2cli/issues/338>`_)
* Add warning and error handling for `ifcfg` import on Windows and OSX (`#332 <https://github.com/ros2/ros2cli/issues/332>`_)
* Add RMW name to report  (`#335 <https://github.com/ros2/ros2cli/issues/335>`_)
* Make network check case-insensitive (`#334 <https://github.com/ros2/ros2cli/issues/334>`_)
* install package manifest (`#330 <https://github.com/ros2/ros2cli/issues/330>`_)
* update README entry point examples (`#329 <https://github.com/ros2/ros2cli/issues/329>`_)
* Update report feature with new argument, add temp fix for ifcfg module  (`#324 <https://github.com/ros2/ros2cli/issues/324>`_)
* removing ifcfg_vendor (`#323 <https://github.com/ros2/ros2cli/issues/323>`_)
* Add network configuration check and report to ros2doctor (`#319 <https://github.com/ros2/ros2cli/issues/319>`_)
* add ros2doctor README (`#318 <https://github.com/ros2/ros2cli/issues/318>`_)
* Add distribution check and report feature to `ros2 doctor` command   (`#311 <https://github.com/ros2/ros2cli/issues/311>`_)
* Contributors: Claire Wang, Dirk Thomas

0.7.4 (2019-05-29)
------------------

0.7.3 (2019-05-20)
------------------

0.7.2 (2019-05-08)
------------------

0.7.1 (2019-04-17)
------------------

0.7.0 (2019-04-14)
------------------

0.6.3 (2019-02-08)
------------------

0.6.2 (2018-12-12)
------------------

0.6.1 (2018-12-06)
------------------

0.6.0 (2018-11-19)
------------------

0.5.4 (2018-08-20)
------------------

0.5.3 (2018-07-17)
------------------

0.5.2 (2018-06-28)
------------------

0.5.1 (2018-06-27 12:27)
------------------------

0.5.0 (2018-06-27 12:17)
------------------------

0.4.0 (2017-12-08)
------------------
