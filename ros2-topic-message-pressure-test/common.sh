#!/bin/bash

DATE=$(TZ='America/Vancouver' date '+%Y-%m-%d')
NOW=$(TZ='America/Vancouver' date '+%Y-%m-%d_%H-%M-%S')

# handle all external overrides
if [ -n "$OVERRIDE_NUMBER" ]; then
  export NUMBER=$OVERRIDE_NUMBER
fi
if [ -n "$OVERRIDE_RATE" ]; then
  export RATE=$OVERRIDE_RATE
fi
if [ -n "$OVERRIDE_OVERLAP_TIME" ]; then
  export OVERLAP_TIME=$OVERRIDE_OVERLAP_TIME
fi
if [ -n "$OVERRIDE_LOG_DIR_POSTFIX" ]; then
  export LOG_DIR_POSTFIX=$OVERRIDE_LOG_DIR_POSTFIX
fi

if [ -n "$OVERRIDE_QOS_DEPTH" ]; then
  export QOS_DEPTH=$OVERRIDE_QOS_DEPTH
fi
if [ -n "$OVERRIDE_QOS_HISTORY" ]; then
  export QOS_HISTORY=$OVERRIDE_QOS_HISTORY
fi
if [ -n "$OVERRIDE_QOS_RELIABILITY" ]; then
  export QOS_RELIABILITY=$OVERRIDE_QOS_RELIABILITY
fi


# OVERRIDE_LOG_ROOT
if [ -n "$OVERRIDE_LOG_ROOT" ]; then
  export LOG_ROOT=$OVERRIDE_LOG_ROOT
else
  export LOG_ROOT=$DATE
fi
# OVERRIDE_LOG_PATH
if [ -n "$OVERRIDE_LOG_PATH" ]; then
  export LOG_PATH=$OVERRIDE_LOG_PATH
else
  export LOG_PATH=$NOW
fi


LOG_CORE_DIR=$LOG_ROOT/$LOG_PATH-[${NUMBER}_${RATE}_${QOS_RELIABILITY}_${QOS_HISTORY}_${QOS_DEPTH}]
if [ -n "$LOG_DIR_POSTFIX" ]; then
  LOG_CORE_DIR=$LOG_CORE_DIR-$LOG_DIR_POSTFIX
fi

export LOG_CORE_PATH=$LOG_CORE_DIR

export LOG_DIR=~/logs/$LOG_CORE_DIR

echo "NUMBER: $NUMBER"
echo "RATE: $RATE"
echo "RMW_IMPLEMENTATION: $RMW_IMPLEMENTATION"
echo "NOW: $NOW"
echo "LOG_DIR: $LOG_DIR"

echo NDDS_QOS_PROFILES:  [$NDDS_QOS_PROFILES]
echo NDDS_DISCOVERY_PEERS:  [$NDDS_DISCOVERY_PEERS]

# export MESSAGE_TYPE="std_msgs/String"
# export MESSAGE="{data: 'ROS2 Standard Message: []'}"
export MESSAGE_TYPE="std_msgs/Header"
export MESSAGE="{stamp: {sec: 123, nanosec: 456}, frame_id: 'This is a demo frame-id'}"

export KEEP_PUB_NODE_ALIVE=10


# geometry_msgs/msg/Twist
# {linear: {x: 1.2, y: 2.3, z: 3.4}, angular: {x: 5.6, y: 6.7, z: 7.8}}

# Function to print the current time with the message
function echo_with_time() {
  echo "$(TZ='America/Vancouver' date '+%Y-%m-%d %H:%M:%S') $1"
}

function detect_msg_loss() {
  local file=$1
  local min=$2
  local max=$3

  # Initialize an empty array
  numbers=()
  loss=()

  # Read the file line by line
  while IFS= read -r line
  do
    # Use grep to check if the line contains the pattern
    if echo "$line" | grep -q 'frame-id\[[0-9]*\]'; then
      # Use sed to extract the number and add it to the array
      numbers+=($(echo "$line" | sed -n 's/.*frame-id\[\([0-9]*\)\].*/\1/p'))
    fi
  done < $file

  # echo "${numbers[@]}"

  for i in $(seq $min $max)
  do
    if [[ ! " ${numbers[@]} " =~ " $i " ]]; then
      loss+=($i)
    fi
  done

  echo ${loss[@]}
}

function format_continuous_numbers() {
  local arr=("$@")
  local prev=${arr[0]}

  for i in ${arr[@]}
  do
    if [ $((i - prev)) -gt 1 ]; then
      # print in a new line for there is a gap
      echo
    fi

    # echo $i in single line
    echo -n "$i "
    prev=$i
  done

  # echo $formatted

  # echo "${arr[@]}"
}

function parse_numbers() {
  local file=$1
  local arr=()

  # read each line of the file
  while IFS= read -r line; do
    # split the line by ":"
    IFS=':' read -ra arr <<< "$line"

    # if array length is greater than 1
    if [ "${#arr[@]}" -gt 1 ]; then
      # get the last element
      
      # trim the leading and trailing spaces
      num=$(echo "${arr[-1]}" | xargs)

      echo $num
    fi
  done < "$file"
}

function sum() {
  local sum=0
  for i in "$@"; do
    sum=$((sum + i))
  done
  echo $sum
}

# if NUMBER and RATE are both defined
if [ -n "$NUMBER" ] && [ -n "$RATE" ]; then
  echo "NUMBER and RATE are defined"

  if [ -f  /opt/ros/foxy/setup.bash ]; then
    source /opt/ros/foxy/setup.bash
  fi

  if [ -f  /opt/rmw_connextdds/setup.bash ]; then
    source /opt/rmw_connextdds/setup.bash
  fi

  if [ -f  ~/scripts/ros2_ws/install/setup.bash ]; then
    source ~/scripts/ros2_ws/install/setup.bash
  fi

  # Create a directory for the default ros2 log files
  if [ ! -d ~/.ros/logs ]; then
      mkdir -p ~/.ros/logs
  fi

  # Create a directory for the ros2 cli output as log files
  if [ ! -d $LOG_DIR ]; then
      mkdir -p $LOG_DIR
  fi
else
  echo "NUMBER and RATE are not defined"
fi
