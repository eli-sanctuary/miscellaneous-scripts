import re
import sys


def detect_msg_loss(file, min, max):
    numbers = []
    loss = []

    # Read the file line by line
    with open(file, 'r') as f:
        for line in f:
            # Use regex to find the pattern
            match = re.search(r'frame-id\[(\d+)\]', line)
            if match:
                numbers.append(int(match.group(1)))

    # Find the missing numbers within the specified range
    for i in range(min, max + 1):
        if i not in numbers:
            loss.append(i)

    return loss


def main():
    if len(sys.argv) != 4:
        print("Usage: python script.py <file> <min> <max>")
        sys.exit(1)

    file = sys.argv[1]
    min = int(sys.argv[2])
    max = int(sys.argv[3])

    lost = detect_msg_loss(file, min, max)
    # print(lost)
    print(' '.join(map(str, lost)))


if __name__ == "__main__":
    main()
