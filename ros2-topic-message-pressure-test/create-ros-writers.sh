#!/bin/bash

# Get the absolute path of the directory containing the script
script_dir="$( cd "$( dirname "${BASH_SOURCE[0]}" )" &> /dev/null && pwd )"

# Source the file using its absolute path
source "$script_dir/settings.sh"
source "$script_dir/common.sh"


# ------------------------------

# to wait for subscriber to be ready
WAIT_BEFORE_TOPIC_PUB=10

PUBLISH_MAX=$(( ($NUMBER + $OVERLAP_TIME ) * $RATE))


PUBLISH_ERROR_TIME=3

CONTINUE_PUBLISHING=false

PUBLISH_RUN_LIMIT_TIME=$(( $NUMBER + $OVERLAP_TIME  + $KEEP_PUB_NODE_ALIVE + 5))

echo "PUBLISH_RUN_LIMIT_TIME: $PUBLISH_RUN_LIMIT_TIME"
echo "PUBLISH_MAX: $PUBLISH_MAX"
echo "CONTINUE_PUBLISHING: $CONTINUE_PUBLISHING"

# Create a directory for the publisher log files
if [ ! -d $LOG_DIR/pub ]; then
    mkdir -p $LOG_DIR/pub
fi

# Create a directory for the RTI log files
if [ ! -d $LOG_DIR/rti_log ]; then
    mkdir -p $LOG_DIR/rti_log
fi

# Remove old log files
rm $LOG_DIR/pub/log_write_*.txt > /dev/null 2>&1

tcpdump -i any -w $LOG_DIR/writer.pcap &
tcpdump_pid=$!

create_topic() {
  i=$1
  output_file=$LOG_DIR/pub/log_write_$i.txt

  # Maximum number of attempts
  max_attempts=5
  attempt=1

  # Loop until the command succeeds or maximum attempts reached
  while [ $attempt -le $max_attempts ]; do
    start=$(date +%s)

    if [ $CONTINUE_PUBLISHING = true ]; then
      echo_with_time "Try to continuous publish topic /topic/test$i (max: $PUBLISH_MAX)"
      LOG_FILE_ID=writer-pub-$i ros2 topic pub --times $PUBLISH_MAX --rate $RATE --keep-alive $KEEP_PUB_NODE_ALIVE --qos-depth $QOS_DEPTH --qos-history $QOS_HISTORY --qos-reliability $QOS_RELIABILITY /topic/test$i "$MESSAGE_TYPE" "$MESSAGE" >> $output_file
      error_code=$?
    else
      echo_with_time "Try to publish topic /topic/test$i (max: $PUBLISH_MAX) in no more than $PUBLISH_RUN_LIMIT_TIME seconds"
      LOG_FILE_ID=writer-pub-$i timeout --preserve-status --foreground --verbose --kill-after=20s $PUBLISH_RUN_LIMIT_TIME \
      ros2 topic pub --times $PUBLISH_MAX --rate $RATE --keep-alive $KEEP_PUB_NODE_ALIVE --qos-depth $QOS_DEPTH --qos-history $QOS_HISTORY --qos-reliability $QOS_RELIABILITY /topic/test$i "$MESSAGE_TYPE" "$MESSAGE" >> $output_file
      error_code=$?
    fi

    end=$(date +%s)

    # Calculate elapsed time
    elapsed=$((end - start))

    if [ $elapsed -lt $PUBLISH_ERROR_TIME ]; then
      # ros2 topic pub finished too quick, it should not because of manully Ctrl+C, consider to try again
      # Check the return value
      if [ $error_code -eq 0 ]; then
        echo_with_time "Topic /topic/test$i published successfully."
        break  # Exit the loop if the command succeeds
      else
        echo_with_time "Failed to publish topic /topic/test$i. Retrying...$attempt/$max_attempts"
        ((attempt++))  # Increment the attempt counter
      fi
    else
      echo_with_time "Topic /topic/test$i has published for $elapsed seconds. (error_code: $error_code)"

      # Count the number of lines containing "publishing #" in a file
      count=$(grep -c "publishing #" $output_file)

      echo -e "publish into /topic/test$i \t: $count" >> $LOG_DIR/writer-count.txt
      echo_with_time "Writer $i Count: $count"
      break
    fi
  done
}

create_topic_one_by_one() {
  i=$1
  output_file=$LOG_DIR/pub/log_write_$i.txt

  # Maximum number of attempts
  max_attempts=5
  attempt=1
  counter=1
 
  begin=$(date +%s)
  while [  $(( $(date +%s) - $begin )) -lt $PUBLISH_RUN_LIMIT_TIME ]; do

    # Loop until the command succeeds or maximum attempts reached
    while [ $attempt -le $max_attempts ]; do
      start=$(date +%s)

      # e.g.
      # ros2 topic pub --rate $RATE --keep-alive $KEEP_PUB_NODE_ALIVE --qos-depth $QOS_DEPTH --qos-history $QOS_HISTORY --qos-reliability $QOS_RELIABILITY /topic/test1 "std_msgs/String" "{data: 'ROS2 Standard Message: 001'}"

      echo_with_time "Try to continuous publish topic /topic/test$i, counter: $counter"
      LOG_FILE_ID=writer-pub-$i ros2 topic pub --once --keep-alive $KEEP_PUB_NODE_ALIVE --qos-depth $QOS_DEPTH --qos-history $QOS_HISTORY --qos-reliability $QOS_RELIABILITY /topic/test$i $MESSAGE_TYPE "{data: 'ROS2 Standard Message: $counter'}" >> $output_file
      error_code=$?

      end=$(date +%s)

      # Calculate elapsed time
      elapsed=$((end - start))
  
      ((counter++))

      if [ $elapsed -lt $PUBLISH_ERROR_TIME ]; then
        # ros2 topic pub finished too quick, it should not because of manully Ctrl+C, try again
        # Check the return value
        if [ $error_code -eq 0 ]; then
          echo_with_time "Topic /topic/test$i published successfully."
          break  # Exit the loop if the command succeeds
        else
          echo_with_time "Failed to publish topic /topic/test$i. Retrying...$attempt/$max_attempts"
          ((attempt++))  # Increment the attempt counter
        fi
      else
        echo_with_time "Topic /topic/test$i published for $elapsed seconds. (error_code: $error_code)"
        break
      fi

    done

  done

}

if [ $WAIT_BEFORE_TOPIC_PUB -gt 0 ]; then
  echo_with_time "Wait for $WAIT_BEFORE_TOPIC_PUB seconds, to run ros2 topic pub"
  sleep $WAIT_BEFORE_TOPIC_PUB
fi

echo_with_time "Start the commands in the background"
for ((i=1; i<=$NUMBER; i++))
do
  # add a delay to avoid all the publishers start at the same time
  sleep 1

  create_topic $i &
done

echo "Background jobs: "
echo "$(jobs -p | tr '\n' ' ')"

sleep 5

if [ $CONTINUE_PUBLISHING = true ]; then
  echo "Press any key to stop publishing."
  read -n 1 -s -r

  echo_with_time "Stop all background writer jobs:"
  echo "$(jobs -p | tr '\n' ' ')"
  for job in `jobs -p`
  do
    kill -SIGINT $job
  done
else
  # echo_with_time "All topics should keep publishing for $PUBLISH_RUN_LIMIT_TIME seconds"
  # sleep $PUBLISH_RUN_LIMIT_TIME
  echo_with_time "All topics are publishing ... waiting them to finish"

  # wait

  # Wait for all processes except the one you want to skip
  for pid in $(jobs -p); do
      if [ $pid -ne $tcpdump_pid ]; then
          wait $pid
      fi
  done

  kill -SIGINT $tcpdump_pid

  echo_with_time "--------------------------"
  echo_with_time "All background processes have finished"
  echo_with_time "All topics are done publishing"
fi

env | sort > $LOG_DIR/writer_env.txt
LOG_FILE_ID=writer-doctor ros2 doctor --report  > $LOG_DIR/pub/ros2-doctor.txt

ipcs -a > $LOG_DIR/writer-ipcs.txt

echo_with_time "Exiting Writer"
